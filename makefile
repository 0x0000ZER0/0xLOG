
test:
	gcc -O2 test.c z0_log.c -Wall -pedantic -o 0xLOG_test -I0xTEST

debug:
	gcc -g test.c z0_log.c -DZ0_DEBUG -Wall -pedantic -o 0xLOG_test -I0xTEST

clear:
	rm ./0xLOG_test ./log.txt
