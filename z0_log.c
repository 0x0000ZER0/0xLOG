#include "z0_log.h"

#include <stdio.h>
#include <string.h>
#include <malloc.h>

int   _z0_log_types_ = 0x00;
char *_z0_log_path_  = NULL;

void
z0_log_init(int types) {
	_z0_log_types_ = types;

#ifdef Z0_DEBUG
	printf("DEBUG (TYPES): %i\n", _z0_log_types_);
#endif	
}

void
z0_log_free() {
	free(_z0_log_path_);
}

void
z0_log_path(char *path) {
	_z0_log_path_ = strdup(path);

#ifdef Z0_DEBUG
	printf("DEBUG (PATH):  %s\n", _z0_log_path_);
#endif	
}

void
z0_log_info(char *txt, int len) {
	if (_z0_log_types_ & Z0_LOG_CONSOLE)
		printf("INFO: %.*s\n", len, txt);

	if (_z0_log_types_ & Z0_LOG_FILE) {
		FILE *hnd;
		hnd = fopen(_z0_log_path_, "a");

		fprintf(hnd, "INFO: %.*s\n", len, txt);

		fclose(hnd);
	} 
}

void
z0_log_warn(char *txt, int len) {
	if (_z0_log_types_ & Z0_LOG_CONSOLE)
		printf("WARN: %.*s\n", len, txt);

	if (_z0_log_types_ & Z0_LOG_FILE) {
		FILE *hnd;
		hnd = fopen(_z0_log_path_, "a");

		fprintf(hnd, "WARN: %.*s\n", len, txt);

		fclose(hnd);
	} 
}

void
z0_log_err(char *txt, int len) {
	if (_z0_log_types_ & Z0_LOG_CONSOLE)
		printf("ERR:  %.*s\n", len, txt);

	if (_z0_log_types_ & Z0_LOG_FILE) {
		FILE *hnd;
		hnd = fopen(_z0_log_path_, "a");

		fprintf(hnd, "ERR:  %.*s\n", len, txt);

		fclose(hnd);
	} 
}

void
z0_log_write(int lvl, char *txt, int len) {
	switch (lvl) {
	case Z0_LOG_INFO:
		z0_log_info(txt, len);
		break;
	case Z0_LOG_WARN:
		z0_log_warn(txt, len);
		break;
	case Z0_LOG_ERR:
		z0_log_err(txt, len);
		break;
	}
}

void
z0_log_info_term(char *txt) {
	if (_z0_log_types_ & Z0_LOG_CONSOLE)
		printf("INFO: %s\n", txt);

	if (_z0_log_types_ & Z0_LOG_FILE) {
		FILE *hnd;
		hnd = fopen(_z0_log_path_, "a");

		fprintf(hnd, "INFO: %s\n", txt);

		fclose(hnd);
	} 
}

void
z0_log_warn_term(char *txt) {
	if (_z0_log_types_ & Z0_LOG_CONSOLE)
		printf("WARN: %s\n", txt);

	if (_z0_log_types_ & Z0_LOG_FILE) {
		FILE *hnd;
		hnd = fopen(_z0_log_path_, "a");

		fprintf(hnd, "WARN: %s\n", txt);

		fclose(hnd);
	} 
}

void
z0_log_err_term(char *txt) {
	if (_z0_log_types_ & Z0_LOG_CONSOLE)
		printf("ERR:  %s\n", txt);

	if (_z0_log_types_ & Z0_LOG_FILE) {
		FILE *hnd;
		hnd = fopen(_z0_log_path_, "a");

		fprintf(hnd, "ERR:  %s\n", txt);

		fclose(hnd);
	} 
}

void
z0_log_write_term(int lvl, char *txt) {
	switch (lvl) {
	case Z0_LOG_INFO:
		z0_log_info_term(txt);
		break;
	case Z0_LOG_WARN:
		z0_log_warn_term(txt);
		break;
	case Z0_LOG_ERR:
		z0_log_err_term(txt);
		break;
	}
}

