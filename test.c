#include "z0_log.h"
#include "z0_test.h"

#include <string.h>

void
test_set_types() {
	Z0_TEST_NAME("TEST SET TYPES");

	z0_log_init(Z0_LOG_CONSOLE);
	Z0_TEST_ASSERT(_z0_log_types_ & Z0_LOG_CONSOLE);
	//reset
	_z0_log_types_ = 0;
	
	z0_log_init(Z0_LOG_FILE);
	Z0_TEST_ASSERT(_z0_log_types_ & Z0_LOG_FILE);
	//reset
	_z0_log_types_ = 0;
	
	z0_log_init(Z0_LOG_CONSOLE | Z0_LOG_FILE);
	Z0_TEST_ASSERT((_z0_log_types_ & Z0_LOG_CONSOLE) &&
		       (_z0_log_types_ & Z0_LOG_FILE));
	//reset
	_z0_log_types_ = 0;
}

void
test_free_path() {
	Z0_TEST_NAME("TEST FREE PATH");

	z0_log_free();
	Z0_TEST_ASSERT(1);
}

void
test_set_path() {
	Z0_TEST_NAME("TEST SET PATH");
	
	int res;

	z0_log_path("test.txt");
	res = strcmp(_z0_log_path_, "test.txt");
	Z0_TEST_ASSERT(res == 0);

	z0_log_free();
}

void
test_info_log() {
	Z0_TEST_NAME("TEST INFO LOG");

	z0_log_init(Z0_LOG_CONSOLE);
	z0_log_info("test 001", 8);
	z0_log_info_term("test 002");
}

void
test_warn_log() {
	Z0_TEST_NAME("TEST WARN LOG");

	z0_log_init(Z0_LOG_CONSOLE);
	z0_log_warn("test 001", 8);
	z0_log_warn_term("test 002");
}

void
test_err_log() {
	Z0_TEST_NAME("TEST ERR LOG");

	z0_log_init(Z0_LOG_CONSOLE);
	z0_log_err("test 001", 8);
	z0_log_err_term("test 002");
}

void
test_write_log() {
	Z0_TEST_NAME("TEST WRITE LOG");

	z0_log_init(Z0_LOG_CONSOLE);
	z0_log_write(Z0_LOG_INFO, "test 001", 8);
	z0_log_write(Z0_LOG_WARN, "test 002", 8);
	z0_log_write(Z0_LOG_ERR, "test 003", 8);
	z0_log_write_term(Z0_LOG_INFO, "test 004");
	z0_log_write_term(Z0_LOG_WARN, "test 005");
	z0_log_write_term(Z0_LOG_ERR, "test 006");
}

void
test_log_file() {
	Z0_TEST_NAME("TEST LOG FILE");

	z0_log_init(Z0_LOG_FILE);
	z0_log_path("log.txt");

	z0_log_info("test 001", 8);
	z0_log_info_term("test 002");
	z0_log_warn("test 003", 8);
	z0_log_warn_term("test 004");
	z0_log_err("test 005", 8);
	z0_log_err_term("test 006");
	
	z0_log_free();

	Z0_TEST_ASSERT(1);
}

void
test_all_types() {
	Z0_TEST_NAME("TEST ALL TYPES");

	z0_log_init(Z0_LOG_FILE | Z0_LOG_CONSOLE);
	z0_log_path("log.txt");

	z0_log_info("test 001", 8);
	z0_log_info_term("test 002");
	z0_log_warn("test 003", 8);
	z0_log_warn_term("test 004");
	z0_log_err("test 005", 8);
	z0_log_err_term("test 006");
	
	z0_log_free();
}

int
main() {
	test_set_types();	
	test_free_path();
	test_set_path();
	test_info_log();
	test_warn_log();
	test_err_log();
	test_write_log();
	test_log_file();
	test_all_types();
	
	return 0;
}
