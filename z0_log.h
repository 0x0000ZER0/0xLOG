#ifndef Z0_LOG_H
#define Z0_LOG_H

#define Z0_LOG_CONSOLE	0x01
#define Z0_LOG_FILE	0x02

#define Z0_LOG_INFO 0x1
#define Z0_LOG_WARN 0x2
#define Z0_LOG_ERR  0x3

extern int   _z0_log_types_;
extern char *_z0_log_path_;

void
z0_log_init(int);

void
z0_log_free();

void
z0_log_path(char*);

void
z0_log_info(char*, int);

void
z0_log_warn(char*, int);

void
z0_log_err(char*, int);

void
z0_log_write(int, char*, int);

void
z0_log_info_term(char*);

void
z0_log_warn_term(char*);

void
z0_log_err_term(char*);

void
z0_log_write_term(int, char*);

#endif
