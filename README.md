# 0xLOG

A logging library written in `C`.

### API

- There are two output types: `stdout` and custom file. To initialize one of them (or both), you have to use the `z0_log_init` function.

```c
// enables logging only to [stdout].
z0_log_init(Z0_LOG_CONSOLE);

// enables logging only to custom file handler.
z0_log_init(Z0_LOG_FILE);

// enables both.
z0_log_init(Z0_LOG_CONSOLE | Z0_LOG_FILE);
```

- If the "logging to a file" is enabled, then the file path must be specified. Note that the string will be allocated dynamically so at the end the user has to call the `z0_log_free` function in order to clean up the allocated string.

```c
z0_log_init(Z0_LOG_FILE);
z0_log_path("log.txt");

//...

z0_log_free();
```
- There are 3 log levels: `INFO`, `WARNING` and `ERROR`. You can call them with following functions. Note that the caller must specify the string length. There are alternative versions of those functions that you can find in the next point. 

```c
z0_log_info("test 001", 8);
z0_log_warn("test 002", 8);
z0_log_err("test 003", 8);

//will output:

// INFO: test 001
// WARN: test 002
// ERR:  test 003
```

- For the null-terminated strings you can use the same functions with `_term` postfix:

```c
z0_log_info_term("test 001");
z0_log_warn_term("test 002");
z0_log_err_term("test 003");

//will output:

// INFO: test 001
// WARN: test 002
// ERR:  test 003
```

- There is also a more general function where you can specifiy the log level explicitly. There is also the null-terminated string version of it:

```c
z0_log_write(Z0_LOG_INFO, "test 001", 8);
z0_log_write(Z0_LOG_WARN, "test 002", 8);
z0_log_write(Z0_LOG_ERR, "test 003", 8);
z0_log_write_term(Z0_LOG_INFO, "test 004");
z0_log_write_term(Z0_LOG_WARN, "test 005");
z0_log_write_term(Z0_LOG_ERR, "test 006");

//will output:

// INFO: test 001
// WARN: test 002
// ERR:  test 003
// INFO: test 004
// WARN: test 005
// ERR:  test 006
```
